import { CUIPage } from './app.po';

describe('cui App', () => {
  let page: CUIPage;

  beforeEach(() => {
    page = new CUIPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
