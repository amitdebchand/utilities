import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


//Third party
import { CKEditorModule } from 'ng2-ckeditor';

//Components
import { HomeComponent } from './User/home/home.component';
import { EasyEnglishComponent } from './User/English/easy-english/easy-english.component';

//Services
import { EasyEnglishService } from './User/English/easy-english/easy-english.service';
import { MyDictionaryComponent } from './User/English/my-dictionary/my-dictionary.component';

@NgModule({
  declarations: [
     HomeComponent,
     EasyEnglishComponent,
     MyDictionaryComponent
  ],
  imports: [
    BrowserModule,
    NgxJsonViewerModule,
    FormsModule,
    HttpModule,
    CKEditorModule 
  ],
  providers: [EasyEnglishService],
  bootstrap: [ MyDictionaryComponent ]  // [HomeComponent]
})
export class AppModule { }
