import { Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
export class DataService {

    constructor(private url: string, private http: Http){

    }

    getAll(){
        return this.http.get(this.url).catch(this.handleError);
    }






    private handleError(error: Response){
        
         if(error.status === 404){
             return Observable.throw(new Error("404 Resource not found."));
         }else{
             return Observable.throw(new Error("Application level generic error"));
         }
    }

}