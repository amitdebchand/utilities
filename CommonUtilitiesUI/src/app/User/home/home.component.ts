import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  sai = {'name': 'Sai', 'role': 786, "student":{'name' :  'Narendra', 'child':{'ch':{'c':'a'}}}, isGod : true, devoteesName : ['Amit', 'Whole world'], 'another': {'name': 'Sai', 'role': 786, "student":{'name' :  'Narendra', 'child':{'ch':{'c':'a'}}}, isGod : true, devoteesName : ['Amit', 'Whole world']} };
  code = {'sai': true};
   
  constructor() { }

  ngOnInit() {
     this.printOutput();
  }

  
   obj = {a:1, 'b':'foo', c:[false,'false',null, 'null', {d:{e:1.3e5,f:'1.3e5'}}]};
   str = JSON.stringify(this.obj, undefined, 4);

  syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

  
    printOutput(){   
      
     document.getElementById('output').innerHTML= this.syntaxHighlight(this.str);
      
    }

}
