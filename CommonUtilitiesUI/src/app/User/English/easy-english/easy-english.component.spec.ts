import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EasyEnglishComponent } from './easy-english.component';

describe('EasyEnglishComponent', () => {
  let component: EasyEnglishComponent;
  let fixture: ComponentFixture<EasyEnglishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EasyEnglishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EasyEnglishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
