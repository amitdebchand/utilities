import { Component, OnInit } from '@angular/core';
import { EasyEnglishService } from './easy-english.service';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from 'ng2-ckeditor';

import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';



@Component({
  selector: 'app-easy-english',
  templateUrl: './easy-english.component.html',
  styleUrls: ['./easy-english.component.css']
})
export class EasyEnglishComponent implements OnInit {
 
  public splittedText;
  public selectedWordDisplay: string;
  public visitedWords = [];
  public isReadMode = false;
  public content;
  public config;
 
  ngOnInit() {

  }
 
  constructor(private easyEnglishService: EasyEnglishService) {
 
    this.config = {
      //extraPlugins: 'codesnippet',
      codeSnippet_theme: 'monokai_sublime',
      height: 200

    }

  }

  read(sourceText) {

    this.splittedText = sourceText.trim().split(" ");
    this.isReadMode = true;

  }


  read123(sourceText) {
    this.splittedText = this.content.trim().split(" ");;
    this.isReadMode = true;
  }

  clear(source: HTMLTextAreaElement) {
    source.value = "";
    this.splittedText = [];
    this.visitedWords = [];
  }

  getMeaning(selectedWord: string) {

    this.easyEnglishService.getAll().subscribe(
      (res: Response) => console.log(res.json()),
      (err: Response) => console.log(err)
    );

    this.selectedWordDisplay = this.removeAllSpecialCharacters(selectedWord.toLowerCase());
    this.visitedWords.push(this.selectedWordDisplay);
  }

  private removeAllSpecialCharacters(word): string {
    return word.replace(/[^a-zA-Z]/g, "");
  }

  showTextEditer() {
    this.isReadMode = false;
  }

}

// this.easyEnglishService.getAll()
//       .subscribe(
//         (response: Response)=>console.log(response.json()),
//         (error: Response)=>{ console.log(error)}
// );