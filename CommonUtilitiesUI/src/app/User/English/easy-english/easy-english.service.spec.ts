import { TestBed, inject } from '@angular/core/testing';

import { EasyEnglishService } from './easy-english.service';

describe('EasyEnglishService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EasyEnglishService]
    });
  });

  it('should be created', inject([EasyEnglishService], (service: EasyEnglishService) => {
    expect(service).toBeTruthy();
  }));
});
