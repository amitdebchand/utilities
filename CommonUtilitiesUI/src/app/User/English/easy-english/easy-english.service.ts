import { Injectable } from '@angular/core';
import { DataService } from '../../../common/data-service';
import { Http } from '@angular/http';

@Injectable()
export class EasyEnglishService extends DataService {

  constructor(http: Http) { 
    super("https://jsonplaceholder.typicode.com/users", http);
  }

}
