import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-my-dictionary',
  templateUrl: './my-dictionary.component.html',
  styleUrls: ['./my-dictionary.component.scss']
})
export class MyDictionaryComponent implements OnInit {
 
  public activeTab = "SearchPanel";
  public wordToSearch="";

  constructor() { }
  ngOnInit() {
  }
  
  setTabActive(selectedTab){
     this.activeTab = selectedTab;
  }

  searchMeaningForWord() {
    alert(this.wordToSearch);
  }

}
