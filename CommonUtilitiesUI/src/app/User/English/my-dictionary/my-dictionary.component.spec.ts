import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDictionaryComponent } from './my-dictionary.component';

describe('MyDictionaryComponent', () => {
  let component: MyDictionaryComponent;
  let fixture: ComponentFixture<MyDictionaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDictionaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDictionaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
