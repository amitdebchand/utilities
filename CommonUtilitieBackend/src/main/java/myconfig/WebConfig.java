package myconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.mongodb.MongoClient;


@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "com")
public class WebConfig {

	@Bean public MongoDbFactory mongoDbFactory() {
		
		 UserCredentials userCredentials = new UserCredentials("joe", "secret");
		
		return new SimpleMongoDbFactory(new MongoClient("127.0.0.1",27017), "database");
 	}
	
	
	@Bean
	public MongoTemplate mongoTemplate() {
		return new MongoTemplate(mongoDbFactory());
	}
	
	
	
	
}
