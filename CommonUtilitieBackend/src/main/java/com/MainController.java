package com;

import org.springframework.http.MediaType;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
 
@Controller
@RequestMapping(value = "/main", method = RequestMethod.GET)
public class MainController {

	// @Autowired
	// UserService userService;

	@RequestMapping(value = "/getData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Student getData() {

		Student std = new Student();

		std.setRoll(786);
		std.setName("Sai");

		return std;

	}

}